#! /usr/bin/env python3

location = [0,0]

with open("input.txt") as f:
	for line in f:
		l = line.split(" ")
		if "forward" in l[0]:
			location[0] += int(l[1])
		elif "down" in l[0]:
			location[1] += int(l[1])
		elif "up" in l[0]:
			location[1] -= int(l[1])

print(location)
print(location[0]*location[1])

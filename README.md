# Advent of Code 2021

Advent of code is a yearly advent calendar with new programming problems each day.
This repository contains my solutions to the [AoC 2021 problems](https://adventofcode.com/2021).

My 2020 solutions can be found [here](https://gitlab.com/pavelow/adventofcode2020).


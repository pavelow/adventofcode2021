#! /usr/bin/env python3

increase_ct = 0
data = []

with open("input.txt") as f:
    data = [int(n) for n in f]

last = sum(data[0:3])

for i in range(1, len(data)-2):
    s = sum(data[i:i+3])
    if s > last:
        increase_ct += 1
    last = s

print(increase_ct)

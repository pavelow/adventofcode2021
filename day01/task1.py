#! /usr/bin/env python3

increase_ct = 0

with open("input.txt") as f:
    last = int(f.readline())
    for line in f:
        line = int(line)
        if line > last:
            increase_ct += 1
        last = line

print(increase_ct)
